\babel@toc {slovak}{}
\contentsline {chapter}{\numberline {1}\IeC {\'U}vod}{2}{chapter.1}
\contentsline {chapter}{\numberline {2}Form\IeC {\'a}lny a neform\IeC {\'a}lny popis}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Neform\IeC {\'a}lny popis}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}Form\IeC {\'a}lny popis}{4}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Protokol}{4}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Procesy}{6}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Offline funkcionalita}{8}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Op\IeC {\"a}tovn\IeC {\'e} odosielanie}{9}{subsection.2.2.4}
\contentsline {subsection}{\numberline {2.2.5}O\IeC {\v s}etrenie v\IeC {\'y}padkov pripojenia}{10}{subsection.2.2.5}
\contentsline {subsection}{\numberline {2.2.6}Logovanie}{11}{subsection.2.2.6}
\contentsline {chapter}{\numberline {3}Nahr\IeC {\'a}vanie audia}{12}{chapter.3}
\contentsline {section}{\numberline {3.1}A/D prevodn\IeC {\'\i }k}{12}{section.3.1}
\contentsline {section}{\numberline {3.2}Pulzn\IeC {\'a} k\IeC {\'o}dov\IeC {\'a} modul\IeC {\'a}cia}{12}{section.3.2}
\contentsline {chapter}{\numberline {4}V\IeC {\'y}voj na Androide}{14}{chapter.4}
\contentsline {section}{\numberline {4.1}Nahr\IeC {\'a}vanie v Androide}{14}{section.4.1}
\contentsline {subsubsection}{MediaRecorder}{14}{section*.4}
\contentsline {subsubsection}{AudioRecord}{14}{section*.5}
\contentsline {subsubsection}{Zdroj audia}{15}{section*.6}
\contentsline {subsubsection}{Vzorkovacia frekvencia}{16}{section*.7}
\contentsline {subsubsection}{Zvukov\IeC {\'y} kan\IeC {\'a}l}{16}{section*.8}
\contentsline {subsubsection}{Audio form\IeC {\'a}t}{16}{section*.9}
\contentsline {subsubsection}{Ve\IeC {\v l}kos\IeC {\v t} buffru}{16}{section*.10}
\contentsline {section}{\numberline {4.2}\IeC {\v S}trukt\IeC {\'u}ra projektu}{16}{section.4.2}
\contentsline {section}{\numberline {4.3}Opr\IeC {\'a}vnenia}{18}{section.4.3}
\contentsline {section}{\numberline {4.4}Bluetooth}{20}{section.4.4}
\contentsline {section}{\numberline {4.5}Podpora viacer\IeC {\'y}ch verzi\IeC {\'\i }}{21}{section.4.5}
\contentsline {subsubsection}{Kompatibilita}{21}{section*.16}
\contentsline {chapter}{\numberline {5}Implement\IeC {\'a}cia}{23}{chapter.5}
\contentsline {section}{\numberline {5.1}K\IeC {\'o}dovanie nahr\IeC {\'a}vky}{23}{section.5.1}
\contentsline {section}{\numberline {5.2}Moduly}{24}{section.5.2}
\contentsline {section}{\numberline {5.3}U\IeC {\v z}\IeC {\'\i }vate\IeC {\v l}sk\IeC {\'e} rozhranie}{30}{section.5.3}
\contentsline {subsubsection}{Ukladanie nastaven\IeC {\'\i }}{32}{section*.22}
\contentsline {subsubsection}{Audio level meter}{33}{section*.23}
\contentsline {section}{\numberline {5.4}Bluetooth nahr\IeC {\'a}vanie}{33}{section.5.4}
\contentsline {section}{\numberline {5.5}Server}{34}{section.5.5}
\contentsline {section}{\numberline {5.6}Testovanie}{36}{section.5.6}
\contentsline {chapter}{\numberline {6}Z\IeC {\'a}ver}{42}{chapter.6}
\contentsline {section}{\numberline {6.1}\IeC {\v D}al\IeC {\v s}\IeC {\'\i } v\IeC {\'y}voj}{43}{section.6.1}
\contentsline {chapter}{Literat\IeC {\'u}ra}{44}{chapter*.32}
\contentsfinish 
